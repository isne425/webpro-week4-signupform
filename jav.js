function validate(e)
{

	var forename = document.forms["register"]["forename"].value;
	if(forename==0) // no forename entered
	{
		alert("PLease enter your forename.");
		return false;
	}
	else if(forename.length < 3) // check number of char 
	{
		alert("The forename must contain at least 3 characters.")
		return false;
	}
	else if(forename*1 == forename) // check if it's number
	{
		alert("Number is invalid.");
		return false;
	}

	var surname = document.forms["register"]["surname"].value;
	if(surname==0) // check if no surname entered
	{
		alert("PLease enter your surname.");
		return false;
	}
	else if(surname.length < 3) // check number of char 
	{
		alert("The surname must contain at least 3 characters.")
		return false;
	}
	else if(surname*1 == surname) // check if it's number
	{
		alert("Number is invalid.");
		return false;
	}

	var username = document.forms["register"]["username"].value;
	if(username==0) // check blank
	{
		alert("PLease enter your username.");
		return false;
	}
	if(username.length < 5)
	{
		alert("Username must contain at least 5 characters.")
		return false;
	}
	

	var password = document.forms["register"]["password"].value;
	var check_special = /((?=.*\d)(?=.*[@#$%&_-]))/;
	if(password==0) // check blank
	{
		alert("PLease enter your password.");
		return false;
	}
	if(password.length <8) // number of alp check
	{
		alert("Password must contain at least 8 characters.")
		return false;
	}
	else if(password.search(/[a-z]/) < 0) // lower case alp check
	{
		alert("Password: Lower case alphabet is required");
		return false;
	}
	else if(password.search(/[A-Z]/) < 0) // upper case alp check
	{
		alert("Password: Upperer case alphabet is required.");
		return false;
	}
	else if(password.search(/[0-9]/) < 0) // number check
	{
		alert("Password: Number is required.");
		return false;
	}
	else if(password.search(/((?=.*\d)(?=.*[@#$%&!_-]))/) < 0) // check symbol
	{
		alert("Password: Symbol is required.")
		return false;
	}

	// re-enter passwoed
	var repass = document.forms["register"]["repass"].value;
	if(repass==0) // check blank
	{
		alert("Please re-enter your password");
		return false;
	}
	if(repass != password) // check matching
	{
		alert("Password is not match. Tyr again.");
		return false;
	}


	var age = document.forms["register"]["age"].value;
	if(age < 18 || age>110)
	{
		alert("Invalid age.");
		return false;
	}

	var email = document.forms["register"]["email"].value;
	if(email==0) // check blank
	{
		alert("Please enter your email.");
		return false;
	}
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        alert("Not a valid e-mail address");
        return false;
    }
	

}
